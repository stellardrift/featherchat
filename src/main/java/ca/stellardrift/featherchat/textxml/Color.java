/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.ComponentBuilder;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({Color.C.class, Color.Colour.class})
@XmlRootElement
public class Color extends Element {

    @XmlAttribute
    @Nullable
    private String name;

    @XmlAttribute
    @Nullable
    String n;

    public Color() {
    }

    public Color(TextColor color) {
        this.name = color instanceof NamedTextColor ? NamedTextColor.NAMES.key((NamedTextColor) color) : color.asHexString();
    }

    @Override
    protected void modifyBuilder(ComponentBuilder<?, ?> builder) {
        if (this.name == null && this.n != null) {
            this.name = this.n;
            this.n = null;
        }

        if (this.name != null) {
            final @Nullable NamedTextColor color = NamedTextColor.NAMES.value(this.name);
            if (color != null) {
                builder.color(color);
            } else {
                builder.color(TextColor.fromCSSHexString(this.name));
            }
        }
    }

    @XmlRootElement
    public static class C extends Color {
        public C() {
        }

        public C(TextColor color) {
            this.n = color instanceof NamedTextColor ? NamedTextColor.NAMES.key((NamedTextColor) color) : color.toString();
        }

    }

    @XmlRootElement
    public static class Colour extends Color {
        public Colour() {

        }
    }
}
