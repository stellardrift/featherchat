/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;


import org.checkerframework.checker.nullness.qual.Nullable;

import static java.util.Objects.requireNonNull;

public class MessageParseException extends RuntimeException {
    private static final long serialVersionUID = 377534925055666339L;
    private final String inputString;
    private final int position;

    public MessageParseException(final String message) {
        this(message, null);
    }

    public MessageParseException(final Throwable cause) {
        this(null, cause);
    }

    public MessageParseException(final @Nullable String message, final @Nullable Throwable cause) {
        super(message, cause);
        this.inputString = "";
        this.position = -1;
    }

    public MessageParseException(final String input, final int position, final String message, final Throwable cause) {
        super(message, cause);
        this.inputString = requireNonNull(input, "input");
        this.position = position;
    }

    public String inputString() {
        return this.inputString;
    }

    public int position() {
        return this.position;
    }
}
