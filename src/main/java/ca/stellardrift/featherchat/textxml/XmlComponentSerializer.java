/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.ComponentSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * TextXML format serializer for Text instances.
 */
public class XmlComponentSerializer implements ComponentSerializer<Component, Component, String> {
    private static final XmlComponentSerializer INSTANCE = new XmlComponentSerializer();

    private static final JAXBContext CONTEXT;
    private static final Pattern NEWLINE = Pattern.compile("\r?\n");

    static {
        try {
            CONTEXT = JAXBContext.newInstance(Element.class);
        } catch (JAXBException e) {
            ExceptionInInitializerError err = new ExceptionInInitializerError("Error creating JAXB context: " + e);
            err.initCause(e);
            throw err;
        }
    }

    public static XmlComponentSerializer get() {
        return INSTANCE;
    }

    private XmlComponentSerializer() {}

    @Override
    public String serialize(Component text) {
        final StringWriter writer = new StringWriter();
        try {
            Marshaller marshaller = CONTEXT.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(Element.fromComponent(text), writer);
        } catch (JAXBException | IOException e) {
            throw new MessageParseException(e);
        }
        return writer.getBuffer().toString();
    }


    /**
     * Also courtesy of http://jazzjuice.blogspot.de/2009/06/jaxb-xmlmixed-and-white-space-anomalies.html
     */
    @SuppressWarnings("unchecked")
    private static <T> T unmarshal(JAXBContext ctx, String xmlData, boolean whitespaceAware) throws Exception {
        UnmarshallerHandler unmarshaller = ctx.createUnmarshaller().getUnmarshallerHandler();
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        XMLReader reader = parser.getXMLReader();
        reader.setContentHandler(whitespaceAware ? new WhitespaceAwareUnmarshallerHandler(unmarshaller) : unmarshaller);
        reader.setErrorHandler(new DefaultHandler());
        reader.parse(new InputSource(new StringReader(xmlData)));
        return (T) unmarshaller.getResult();
    }

    @Override
    public Component deserialize(String input) {
        try {
            final Element element = unmarshal(CONTEXT, "<span>" + NEWLINE.matcher(input).replaceAll("<br />") + "</span>", true);
            return element.toComponent().build();
        } catch (MessageParseException e) {
            throw e;
        } catch (Exception e) {
            throw new MessageParseException(input, 0, "Error parsing TextXML message", e);
        }
    }

    public static <T> Function<T, String> serialized(Function<T, Component> providerFunc) {
        return it -> get().serialize(providerFunc.apply(it));
    }

}
