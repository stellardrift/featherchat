/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.textxml;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Tr extends Element {

    @SuppressWarnings("NullableProblems")
    @XmlAttribute(required = true)
    private String key;

    public Tr() {}

    public Tr(String key) {
        this.key = key;
    }

    @Override
    protected void modifyBuilder(ComponentBuilder<?, ?> builder) {
        // TODO: get rid of this
    }

    @Override
    public ComponentBuilder<?, ?> toComponent() throws Exception {
        final List<Component> items = new ArrayList<>(this.mixedContent.size());
        for (Object child : this.mixedContent) {
            items.add(builderFromObject(child).build());
        }
        ComponentBuilder<?, ?> builder = Component.translatable().key(this.key).args(items);
        applyTextActions(builder);
        return builder;
    }
}
