/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.textxml.XmlComponentSerializer;
import com.github.mustachejava.Code;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.DefaultMustacheVisitor;
import com.github.mustachejava.FragmentKey;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheException;
import com.github.mustachejava.MustacheNotFoundException;
import com.github.mustachejava.MustacheParser;
import com.github.mustachejava.MustacheVisitor;
import com.github.mustachejava.TemplateContext;
import com.github.mustachejava.codes.ExtendCode;
import com.github.mustachejava.codes.PartialCode;
import com.github.mustachejava.reflect.ReflectionObjectHandler;
import net.kyori.adventure.text.ComponentLike;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A custom mustache factory implementing FeatherChat-specific behaviour
 */
public final class FeatherChatMustacheFactory extends DefaultMustacheFactory implements TemplateProvider {
    private final @Nullable TemplateProvider parent;
    private final Function<String, String> templateLookup;

    public FeatherChatMustacheFactory(Function<String, String> resolver) {
        this(null, resolver);
    }

    public FeatherChatMustacheFactory(final @Nullable TemplateProvider parent, Function<String, String> resolver) {
        super(name -> {
            final String result = resolver.apply(name);
            return result == null || result.isEmpty() ? null : new StringReader(result);
        });
        this.templateLookup = resolver;
        this.parent = parent;
        this.oh = new CustomStringifyObjectHandler(object -> {
            if (object instanceof ComponentLike) {
                return XmlComponentSerializer.get().serialize(((ComponentLike) object).asComponent());
            } else {
                return String.valueOf(object);
            }
        });
    }

    @Override
    public void refreshTemplate(String key) {
        this.mustacheCache.remove(key);
        // Partials don't use the normal mustache cache, so it doesn't matter if the template was cached.
        this.mustacheCache.entrySet().removeIf(entry -> containsTemplate(entry.getValue(), key));
    }

    @Override
    public boolean hasTemplate(String identifier) {
        final String potential = this.templateLookup.apply(identifier);
        return (potential != null && !potential.isEmpty()) || (parent != null && parent.hasTemplate(identifier));
    }

    @Override
    public @Nullable Mustache template(String identifier) {
        try {
            return compile(identifier);
        } catch (MustacheNotFoundException ex) {
            return parent == null ? null : parent.template(identifier);
        }
    }

    /**
     * Compile a standalone fragment.
     *
     * @param template template to compile
     * @param templateName identifier, used for error messages
     * @return Compiled template
     */
    @Override
    public Mustache fragment(String template, String templateName) {
        final TemplateContext ctx = new TemplateContext(MustacheParser.DEFAULT_SM, MustacheParser.DEFAULT_EM, templateName, 0, true);
        return getFragment(new FragmentKey(ctx, template));
    }

    /**
     * In mustache templates, there are a couple ways of sneaking in template references. This is how we find them.
     *
     * @param haystack The code to inspect for potentially containing a template
     * @param needleName Name of the template to search for
     * @return whether the provided haystack contains
     */
    static boolean containsTemplate(Code haystack, @NonNull String needleName) {
        if (haystack instanceof Mustache && haystack.getName().equals(needleName)) {
            return true;
        }
        final Code[] children = haystack.getCodes();
        if (children != null) {
            for (Code child : children) { // hopefully no cycles?
                if (child instanceof WrappingCode) {
                    @Nullable Mustache reference = ((WrappingCode) child).getWrapped();
                    return reference != null && needleName.equals(reference.getName());
                }

                if (containsTemplate(child, needleName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Normally, mustache escapes HTML-like elements of templates.
     * We don't want to do that since we want XML text formatting
     * to be passed through.
     *
     * There's also a bit less opportunity for attacks like
     * JS injection when our XML variant doesn't support scripts.
     *
     * @param value Text to add
     * @param writer Writer to append text to.
     */
    @Override
    public void encode(String value, Writer writer) {
        try {
            writer.append(value);
        } catch (IOException e) {
            throw new MustacheException(e);
        }
    }

    @Override
    public MustacheVisitor createMustacheVisitor() {
        return new MustacheReferenceExposingVisitor(this);
    }


    static final class MustacheReferenceExposingVisitor extends DefaultMustacheVisitor {

        MustacheReferenceExposingVisitor(FeatherChatMustacheFactory df) {
            super(df);
        }

        @Override
        public void partial(TemplateContext tc, String variable) {
            TemplateContext partialTC = new TemplateContext(MustacheParser.DEFAULT_SM, MustacheParser.DEFAULT_EM, tc.file(), tc.line(), tc.startOfLine());
            list.add(new PartialExposure(partialTC, df, variable));
        }

        @Override
        public void extend(TemplateContext templateContext, String variable, Mustache mustache) {
            list.add(new ExtendExposure(templateContext, df, mustache, variable));
        }
    }

    /**
     * Interface for {@link Code} instances that wrap a full other template.
     */
    interface WrappingCode extends Code {
        @Nullable Mustache getWrapped();
    }

    /**
     * Subclass of the standard partial code that exposes the partial template
     */
    static final class PartialExposure extends PartialCode implements WrappingCode {

        PartialExposure(TemplateContext tc, DefaultMustacheFactory cf, String variable) {
            super(tc, cf, variable);
        }

        /**
         * Get the template this one is referring to. Will return null until the code has been initialized
         *
         * @return reference target
         */
        public @Nullable Mustache getWrapped() {
            return this.partial;
        }
    }

    static final class ExtendExposure extends ExtendCode implements WrappingCode {

        ExtendExposure(TemplateContext tc, DefaultMustacheFactory mf, Mustache codes, String name) throws MustacheException {
            super(tc, mf, codes, name);
        }

        @Override
        public @Nullable Mustache getWrapped() {
            return this.partial;
        }
    }

    static class CustomStringifyObjectHandler extends ReflectionObjectHandler {
        private final Function<Object, String> stringifier;

        CustomStringifyObjectHandler(Function<Object, String> stringifier) {
            this.stringifier = requireNonNull(stringifier, "stringifier");
        }

        @Override
        public String stringify(Object object) {
            return this.stringifier.apply(object);
        }
    }
}
