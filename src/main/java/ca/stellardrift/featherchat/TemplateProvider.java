/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.textxml.XmlComponentSerializer;
import com.github.mustachejava.Mustache;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Represents an interface that can provide templates to a user
 */
public interface TemplateProvider {
    String TEMPLATE_PLAYER_NAME = "displayname";
    String TEMPLATE_CHAT = "chat";
    String TEMPLATE_EMOTE = "emote";
    String TEMPLATE_CHANNEL_MESSAGE = "channel";
    String TEMPLATE_JOIN = "join";
    String TEMPLATE_QUIT = "quit";
    String TEMPLATE_PING_MOTD = "ping-motd";
    String TEMPLATE_STOP_KICK = "stop-kick";
    String TEMPLATE_TAB_LIST_ENTRY = "tab-list-entry";
    String TEMPLATE_TAB_LIST_HEADER = "tab-list-header";
    String TEMPLATE_TAB_LIST_FOOTER = "tab-list-footer";

    /**
     * Clear any cached values for this template
     * @param identifier template to check
     */
    void refreshTemplate(String identifier);

    boolean hasTemplate(String identifier);

    @Nullable Mustache template(String identifier);

    /**
     * Directly provide a template to process
     * @param template The template to process
     * @return A compiled mustache template
     */
    default Mustache fragment(String template) {
        return fragment(template, "unknown");
    }

    /**
     * Directly provide a template to process
     * @param template The template to process
     * @param templateName An identifier for the template, for use in errors
     * @return A compiled mustache template
     */
    Mustache fragment(String template, String templateName);

    default String processFragmentToString(String fragment, String templateName, Consumer<List<Object>> variablePopulator) {
        final Mustache template = fragment(fragment, templateName);
        return processToString(template, variablePopulator);
    }

    default Component processFragmentToText(String fragment, String templateName, Consumer<List<Object>> variablePopulator) {
        return XmlComponentSerializer.get().deserialize(processFragmentToString(fragment, templateName, variablePopulator));
    }

    /**
     * Process the template with the provided key to a string
     *
     * @param templateKey Template key
     * @param variablePopulator Function that takes a list and submits variables to it
     * @return Processed template, or null if there is no template for the key
     */
    default @Nullable String processToString(String templateKey, Consumer<List<Object>> variablePopulator) {
        final @Nullable Mustache template = template(templateKey);
        if (template == null) {
            return null;
        }
        return processToString(template, variablePopulator);
    }

    /**
     * Process the template with the provided key to a string
     *
     * @param templateKey Template key
     * @param variablePopulator Function that takes a list and submits variables to it
     * @return Processed template, or null if there is no template for the key
     */
    default @Nullable Component processToText(String templateKey, Consumer<List<Object>> variablePopulator) {
        final @Nullable String processed = processToString(templateKey, variablePopulator);
        return processed == null ? null : XmlComponentSerializer.get().deserialize(processed);
    }

    /**
     * Process the template with the provided key to a string
     *
     * @param <E> thrown type
     * @param templateKey Template key
     * @param variablePopulator Function that takes a list and submits variables to it
     * @param exception Exception supplier
     * @return Processed template, or null if there is no template for the key
     * @throws E if no template is available
     */
    default <E extends Exception> Component processOrThrow(String templateKey, Consumer<List<Object>> variablePopulator, Supplier<E> exception) throws E {
        final @Nullable String processed = processToString(templateKey, variablePopulator);
        if (processed == null) {
            throw exception.get();
        }
        return XmlComponentSerializer.get().deserialize(processed);
    }

    /**
     * Process the provided template to a string
     * @param template Template to process
     * @param variablePopulator Accepter of a list of values
     * @return processed template
     */
    static String processToString(Mustache template, Consumer<List<Object>> variablePopulator) {
        final List<Object> vars = new ArrayList<>();
        variablePopulator.accept(vars);

        final StringWriter writer = new StringWriter();
        template.execute(writer, vars);
        return writer.toString();
    }
}
