/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import com.github.mustachejava.Mustache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.spongepowered.configurate.util.UnmodifiableCollections;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FeatherChatMustacheFactoryTest {
    @Test
    public void testResolvingClearsNestedTemplates() {
        final Map<String, String> templateVars = UnmodifiableCollections.buildMap(m -> {
            m.put("name", "moe");
            m.put("age", "245");
        });
        final Map<String, String> templateStore = new HashMap<>();
        templateStore.put("basic", "Hello {{ name }}");
        templateStore.put("expanded", "The computer says: {{> basic}}");

        final FeatherChatMustacheFactory factory = new FeatherChatMustacheFactory(templateStore::get);

        Mustache basicMustache = factory.compile("basic");

        Mustache expandedMustache = factory.compile("expanded");
        assertTrue(FeatherChatMustacheFactory.containsTemplate(expandedMustache, basicMustache.getName()));

        Assertions.assertEquals("The computer says: Hello moe", TemplateProvider.processToString(expandedMustache, vars -> vars.add(templateVars)));

        // Invalidate and recompile
        templateStore.put("basic", "Goodbye {{name }}");
        factory.refreshTemplate("basic");
        expandedMustache = factory.compile("expanded");
        assertEquals("The computer says: Goodbye moe", TemplateProvider.processToString(expandedMustache, vars -> vars.add(templateVars)));
    }
}
