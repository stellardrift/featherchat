import kr.entree.spigradle.kotlin.paper

plugins {
    id("com.github.johnrengelman.shadow")
    id("kr.entree.spigradle") version "2.2.3"
}

val versionPaper = (findProperty("versionPaper") ?: findProperty("versionMinecraft")!!) as String

dependencies {
    implementation("net.kyori:adventure-platform-bukkit:4.0.0-SNAPSHOT") {
        exclude("com.google.code.gson")
    }
    implementation(project(":")) {
        exclude("com.google.guava")
        exclude("com.google.inject")
    }
    shadow(paper("$versionPaper-R0.1-SNAPSHOT"))
}

// Distribution

spigot {
    authors = listOf("zml")
    apiVersion = "1.13"
    commands {
        create("featherchat") {
            description = "FeatherChat test commands"
            permission = "featherchat.test"
        }
    }
}

val relocateRoot = "$group.featherchat.ext"
tasks.shadowJar.configure {
    // minimize()
    mergeServiceFiles()
    listOf("com.github.mustachejava", "net.kyori").forEach {
        // relocate(it, "$relocateRoot.$it")
    }
    dependencies {
        exclude("org.yaml:snakeyaml")
    }
}

tasks.assemble.configure { dependsOn(tasks.shadowJar) }
configurations.archives.configure { extendsFrom(configurations.shadow.get()) }
