import ca.stellardrift.build.configurate.ConfigFormats
import ca.stellardrift.build.configurate.transformations.convertFormat
import ca.stellardrift.build.localization.TemplateType
import ca.stellardrift.build.templating.GenerateTemplateTask
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("com.github.johnrengelman.shadow")
    id("ca.stellardrift.templating")
    id("ca.stellardrift.localization")
    id("ca.stellardrift.configurate-transformations")
}

localization {
    templateFile.set(file("src/main/messages.java.tmpl"))
    templateType.set(TemplateType.JAVA)
}

val versionSponge: String by project

val jdk11 by configurations.registering

dependencies {
    shadow("com.google.auto.value:auto-value-annotations:1.7.4")
    annotationProcessor("com.google.auto.value:auto-value:1.7.4")
    annotationProcessor("com.gabrielittner.auto.value:auto-value-with:1.1.1")
    implementation(project(":")) {
        // Provided by sponge
        exclude("net.kyori")
        exclude("org.spongepowered")

        // Exist in JDK8 already
        exclude("jakarta.xml.bind")
        exclude("com.sun.xml.bind")
    }
    testImplementation(shadow("org.spongepowered:spongeapi:$versionSponge")!!)

    "jdk11"(testImplementation(shadow("org.glassfish.jaxb:jaxb-runtime:2.3.3")!!)!!)
}

tasks {
    withType(GenerateTemplateTask::class).configureEach {
        properties("project" to project,
                "scmUrl" to indra.scm.get().url,
                "license" to indra.license.get(),
                "git" to grgit.describe())
    }

    processResources.configure {
        filesMatching("**/*.yml") {
            expand("project" to project)
            convertFormat(ConfigFormats.YAML, ConfigFormats.JSON)
            name = name.substringBeforeLast('.') + ".json"
        }
    }

    val relocateRoot = "$group.featherchat.ext"
    shadowJar.configure {
        archiveClassifier.set("all")
        minimize()
        listOf("com.github.mustachejava").forEach {
            relocate(it, "$relocateRoot.$it")
        }
    }

    val shadowJdk11 by registering(ShadowJar::class) {
        archiveClassifier.set("jdk11-all")
        from(sourceSets["main"].output)
        minimize()
        configurations = listOf(jdk11.get(), project.configurations.runtimeClasspath.get())

        // jdk11+
        listOf("com.github.mustachejava", "javax.xml.bind", "javax.activation", "com.sun.xml").forEach {
            relocate(it, "$relocateRoot.$it")
        }
        manifest {
            attributes(
                    "Loader" to "java_plain" // declare as a Sponge plugin
            )
        }
    }
    assemble.configure { dependsOn(shadowJar, shadowJdk11) }
}

configurations.archives.configure { extendsFrom(configurations.shadow.get()) }
