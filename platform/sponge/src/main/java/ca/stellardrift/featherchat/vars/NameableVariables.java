/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.util.Nameable;

import java.util.ArrayList;
import java.util.List;

public class NameableVariables {
    private static final List<NamedTextColor> COLORS;

    static {
        COLORS = new ArrayList<>(NamedTextColor.NAMES.values());
        COLORS.remove(NamedTextColor.BLACK);
        COLORS.remove(NamedTextColor.DARK_GRAY);
    }

    private final Nameable nameable;

    public NameableVariables(final Nameable nameable) {
        this.nameable = nameable;
    }

    private NamedTextColor getColorForName(String name) {
        return COLORS.get(Math.abs(name.hashCode() % COLORS.size()));
    }

    public String nameBasedColor() {
        return NamedTextColor.NAMES.key(getColorForName(rawname()));
    }

    public Object nickname() {
        if (this.nameable instanceof Player) {
            final @Nullable Component displayName = ((Player) this.nameable).get(Keys.DISPLAY_NAME).orElse(null);
            return displayName == null ? ("no display name: " + this.nameable.getName()) : displayName;
        }
        return this.nameable.getName();
    }

    public String rawname() {
        return this.nameable.getName();
    }

}
