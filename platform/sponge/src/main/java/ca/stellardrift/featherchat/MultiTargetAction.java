/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.exception.CommandPermissionException;
import org.spongepowered.api.event.Cause;
import org.spongepowered.api.service.permission.Subject;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Utility class to perform operations on multiple targets
 */
public abstract class MultiTargetAction<T extends Audience> {
    private final String basePermission;
    protected final Audience source;
    protected final Subject perm;
    private final Iterable<? extends T> targets;

    protected MultiTargetAction(String basePermission, Cause cause, Iterable<? extends T> targets) {
        this(basePermission, cause.first(Audience.class).get(), cause.first(Subject.class).get(), targets);
    }

    protected MultiTargetAction(String basePermission, Audience source, Subject perm, Iterable<? extends T> targets) {
        this.basePermission = basePermission;
        this.source = source;
        this.perm = perm;
        this.targets = targets;
    }

    public void checkPermission() throws CommandPermissionException {
        boolean selfChecked = false;
        boolean otherChecked = false;
        for (Audience target : targets) {
            if (source == target) {
                if (!selfChecked) {
                    if (!this.perm.hasPermission(basePermission + ".self")) {
                        throw new CommandPermissionException(Messages.COMMANDS_GENERAL_ERROR_PERMISSION.t());
                    }
                    selfChecked = true;
                }
            } else {
                if (!otherChecked) {
                    if (!this.perm.hasPermission(basePermission + ".other")) {
                        throw new CommandPermissionException(Messages.COMMANDS_GENERAL_ERROR_PERMISSION.t());
                    }
                    otherChecked = true;
                }
            }
            if (selfChecked && otherChecked) {
                break;
            }
        }
    }


    @SuppressWarnings({"SuspiciousMethodCalls", "unchecked"})
    public int act() throws CommandException {
        checkPermission();

        Set<T> targets = new HashSet<>();
        for (T target : this.targets) {
            actSingle(target);
            targets.add(target);
        }
        /*
         * Gotta notify source and every target
         * target is 2 states: self and other
         *
         */
        if (targets.size() == 1 && targets.contains(this.source)) {
            this.source.sendMessage(getTargetNotificationMessage((T) this.source));
        } else {
            for (T target : targets) {
                target.sendMessage(getTargetNotificationMessage(target));
            }
            this.source.sendMessage(getSourceNotificationMessage(targets));
        }
        return targets.size();
    }

    public CommandResult actToResult() throws CommandException {
        final int count = this.act();
        return CommandResult.builder().setResult(count).build();
    }

    protected abstract Component getTargetNotificationMessage(T target);
    protected abstract Component getSourceNotificationMessage(Collection<T> targets);

    protected abstract void actSingle(T target);

}
