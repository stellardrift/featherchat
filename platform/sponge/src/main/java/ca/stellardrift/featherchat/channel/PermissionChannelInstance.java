/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import net.kyori.adventure.audience.Audience;
import org.spongepowered.api.Game;

/**
 * A channel that all users receive messages from by default.
 */
public class PermissionChannelInstance extends AbstractChannelInstance {
    private final String qualifier;
    private final Game game;

    public PermissionChannelInstance(Channel config, String qualifier, Game game) {
        super(config);
        this.qualifier = qualifier;
        this.game = game;
    }

    @Override
    public Iterable<Audience> audiences() {
        final String permission = config.permission() + "." + qualifier;
        return this.game.getServer().getOnlinePlayers().stream()
                .filter(it -> it.hasPermission(permission))
                .<Audience>map(it -> it)
                ::iterator;
        // TODO: Reimplement
        /*PermissionService service = game.getServer().getServiceProvider().permissionService();
        return service.getLoadedCollections().values().stream()
                .flatMap(collect -> collect.getLoadedWithPermission(permission).entrySet().stream().filter(Map.Entry::getValue).map(ent -> ent.getKey().getCommandSource()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                ::iterator;*/
    }
}
