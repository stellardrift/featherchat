/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.world.Locatable;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * Channel that filters an existing channel by limiting it to unlocated sources and located sources within a given range
 */
public class RangeChannelInstance extends AbstractChannelInstance {
    private final WeakReference<Locatable> position;
    private final AbstractChannelInstance child;

    public RangeChannelInstance(AbstractChannelInstance child, Locatable source) {
        super(child.config);
        this.position = new WeakReference<>(source);
        this.child = child;
    }

    @Override
    public @Nullable Component transformMessage(Audience recipient, Component text, MessageType type) {
        return this.child.transformMessage(recipient, text, type);
    }

    @Override
    public Iterable<Audience> audiences() {
        final @Nullable Locatable source = this.position.get();
        if (source == null) {
            return Collections.emptySet();
        }
        return StreamSupport.stream(this.child.audiences().spliterator(), false)
                .filter(input -> !(input instanceof Locatable) || ((Locatable) input).getLocation().getPosition()
                    .distanceSquared(source.getLocation().getPosition()) <= config.range() * config.range())
                .collect(Collectors.toList());
    }
}
