/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import com.google.common.collect.ImmutableList;
import org.spongepowered.api.event.Cause;
import org.spongepowered.api.event.impl.AbstractEvent;

import java.util.List;
import java.util.Objects;

public final class VariableGatherEvent extends AbstractEvent {
    private final List<Object> variables;
    private final Cause cause;

    public VariableGatherEvent(Cause cause, List<Object> variables) {
        this.cause = cause;
        this.variables = variables;

    }

    @Override
    public Cause getCause() {
        return this.cause;
    }

    public void addVariables(Object o) {
        variables.add(o);
    }

    public ImmutableList<Object> getVariables() {
        return ImmutableList.copyOf(variables);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VariableGatherEvent)) return false;
        VariableGatherEvent that = (VariableGatherEvent) o;
        return this.variables.equals(that.variables) &&
                getSource().equals(that.getSource()) &&
                getCause().equals(that.getCause());
    }

    @Override
    public int hashCode() {
        return Objects.hash(variables, getSource(), getCause());
    }

    @Override
    public String toString() {
        return "VariableGatherEvent{" +
                "variables=" + variables +
                ", cause=" + cause +
                '}';
    }
}
