/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import ca.stellardrift.featherchat.TemplateProvider;
import ca.stellardrift.featherchat.vars.Variables;
import com.github.mustachejava.Mustache;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.audience.ForwardingAudience;
import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.PlayerChatRouter;

/**
 * Superclass for all FeatherChat channels
 */
public abstract class AbstractChannelInstance implements ForwardingAudience, TemplateProvider, PlayerChatRouter {
    protected final Channel config;

    protected AbstractChannelInstance(Channel config) {
        this.config = config;
    }

    public Channel getConfig() {
        return config;
    }

    @Override
    public void sendMessage(final Identity sender, final @NonNull Component message, final @NonNull MessageType type) {
        for (final Audience aud : this.audiences()) {
            final @Nullable Component transformed = this.transformMessage(aud, message, type);
            if (transformed != null) {
                aud.sendMessage(sender, message, type);
            }
        }
    }

    public @Nullable Component transformMessage(Audience recipient, Component text, MessageType type) {
        try {
            return processOrThrow(TEMPLATE_CHANNEL_MESSAGE, vars -> {
                vars.add(Variables.message(text));
                vars.add(Variables.channelVariables(config.name()));
            }, this::expectTemplate);
        } catch (RuntimeException e) {
            return text;
        }
    }

    @Override
    public void chat(Player player, Component message) {
        this.sendMessage(player, message, MessageType.CHAT);
    }

    protected IllegalStateException expectTemplate() {
        return new IllegalStateException("Template was expected here but none was returned");
    }

    @Override
    public void refreshTemplate(String identifier) {
        config.refreshTemplate(identifier);
    }

    @Override
    public boolean hasTemplate(String identifier) {
        return config.hasTemplate(identifier);
    }

    @Override
    public @Nullable Mustache template(String identifier) {
        return config.template(identifier);
    }

    @Override
    public Mustache fragment(String template, String templateKey) {
        return config.fragment(template, templateKey);
    }
}
