/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.motd;

import ca.stellardrift.featherchat.Commands;
import ca.stellardrift.featherchat.Configuration;
import ca.stellardrift.featherchat.Messages;
import ca.stellardrift.featherchat.Permissions;
import ca.stellardrift.featherchat.TemplateProvider;
import ca.stellardrift.featherchat.vars.VariableGatherEvent;
import ca.stellardrift.featherchat.vars.Variables;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import net.kyori.adventure.text.Component;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.parameter.Parameter;
import org.spongepowered.api.command.parameter.managed.Flag;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.EventManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.api.service.ServiceProvider;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.plugin.PluginContainer;

import java.util.ArrayList;
import java.util.Optional;

import static ca.stellardrift.featherchat.TextFormats.hl;
import static ca.stellardrift.featherchat.TextFormats.normal;
import static ca.stellardrift.featherchat.TextFormats.runCommand;
import static ca.stellardrift.featherchat.TextFormats.suggestCommand;
import static net.kyori.adventure.identity.Identity.nil;
import static net.kyori.adventure.text.Component.text;

@Singleton
public class MotdController {
    private final Configuration config;
    private final EventManager eventManager;
    private final PluginContainer container;
    private final ServiceProvider.GameScoped services;

    @Inject
    MotdController(final Configuration config, final EventManager eventManager, final PluginContainer container, final ServiceProvider.GameScoped services) {
        this.config = config;
        this.eventManager = eventManager;
        this.container = container;
        this.services = services;
    }

    @Listener
    public void onJoin(ServerSideConnectionEvent.Join event) {
        final ServerPlayer ply = event.getPlayer();
        TemplateProvider prov = config.templateProvider(ply);

        for (MotdEntry ent : config.getMotdEntries().values()) {
            if (ent.shouldSendTo(ply)) {
                ply.sendMessage(prov.processFragmentToText(ent.getMessage(), "motd", vars -> {
                    vars.add(Variables.subject(ply));
                    vars.add(Variables.nameable(ply));
                    this.eventManager.post(new VariableGatherEvent(event.getCause(), vars));
                }));
            }
        }
    }

    public Command.Parameterized getMotdCommand() {
        final Parameter.Value<String> motdParam = Parameter.string()
                .setKey("motd")
                .setSuggestions((ctx, input) -> new ArrayList<>(this.config.getMotdEntries().keySet()))
                .build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_MOTD_PARENT_DESCRIPTION.t())
                .setPermission(Permissions.PERM_MOTD_LIST)
                .setExecutor(ctx -> {
                    this.services.paginationService().builder()
                            .header(Messages.COMMANDS_MOTD_LIST_HEADER.t())
                            .contents(config.getMotdEntries().entrySet().stream()
                                    .map(ent -> text().append(hl(runCommand(text().content(ent.getKey()), "/chat motd info " + ent.getKey())))
                                            .append(text(": ")).append(text(ent.getValue().getMessage())).build())
                                    .collect(ImmutableSet.toImmutableSet()))
                            .sendTo(ctx.getCause().getAudience());
                    return CommandResult.success();
                })
                .child(getMotdDeleteCommand(motdParam), "delete")
                .child(getMotdEditCommand(motdParam), "edit", "create", "e")
                .child(getMotdQueryCommand(motdParam), "info", "query")
                .build();
    }

    private Command.Parameterized getMotdQueryCommand(final Parameter.Value<String> motdParam) {
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_MOTD_QUERY_DESCRIPTION.t())
                .setPermission(Permissions.PERM_MOTD_INFO)
                .parameter(motdParam)
                .setExecutor(ctx -> {
                    final String motdArg = ctx.requireOne(motdParam);
                    Commands.checkPermission(ctx, Permissions.PERM_MOTD_EDIT, motdArg);
                    MotdEntry ent = this.config.getMotdEntries().computeIfAbsent(motdArg, key -> new MotdEntry());
                    ctx.sendMessage(nil(), hl(Messages.COMMANDS_MOTD_INFO_HEADER.t()));
                    ctx.sendMessage(nil(), Component.join(Component.space(), hl(suggestCommand(Messages.COMMANDS_MOTD_INFO_MESSAGE.tB(),
                                    "/chat motd edit " + motdArg + " -m \"" + ent.getMessage() + "\"")), normal(text().content(ent.getMessage()))));
                    ctx.sendMessage(nil(), hl(Messages.COMMANDS_MOTD_INFO_PERMISSION.tB(
                            normal(suggestCommand(text().content(String.valueOf(ent.getPermission())),
                                    "/chat motd edit " + motdArg + " -p " + ent.getPermission())))));
                    ctx.sendMessage(nil(), hl(Messages.COMMANDS_MOTD_INFO_ENABLED.tB(
                            normal(suggestCommand(text().content(String.valueOf(ent.isEnabled())),
                                    "/chat motd edit " + motdArg + " -e " + ent.isEnabled())))));
                    return CommandResult.success();
                }).build();
    }

    private Command.Parameterized getMotdEditCommand(final Parameter.Value<String> motdParam) {
        final Parameter.Value<String> permissionParam = Parameter.string().setKey("permission").build();
        final Parameter.Value<String> messageParam = Parameter.string().setKey("message").build();
        final Parameter.Value<Boolean> enabledParam = Parameter.bool().setKey("enabled").build();
        final Flag messageFlag = Flag.builder().alias("message").alias("msg").alias("m").setParameter(messageParam).build();
        final Flag permissionFlag = Flag.builder().alias("permission").alias("perm").alias("p").setParameter(permissionParam).build();
        final Flag enabledFlag = Flag.builder().alias("enabled").alias("active").alias("e").setParameter(enabledParam).build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_MOTD_QUERY_DESCRIPTION.t())
                .setPermission(Permissions.PERM_MOTD_EDIT)
                .flag(messageFlag)
                .flag(permissionFlag)
                .flag(enabledFlag)
                .parameter(motdParam)
                .setExecutor(ctx -> {
                    final String motdArg = ctx.requireOne(motdParam);
                    Commands.checkPermission(ctx, Permissions.PERM_MOTD_EDIT, motdArg);
                    MotdEntry entry = config.getMotdEntries().computeIfAbsent(motdArg, key -> new MotdEntry());
                    final MotdEntry firstEntry = entry;

                    Optional<String> motd = ctx.getOne(messageParam);
                    Optional<String> permission = ctx.getOne(permissionParam);
                    Optional<Boolean> enabled = ctx.getOne(enabledParam);

                    if (motd.isPresent()) {
                        entry = entry.withMessage(motd.get());
                    }
                    if (permission.isPresent()) {
                        entry = entry.withPermission(permission.get());
                    }
                    if (enabled.isPresent()) {
                        entry = entry.withEnabled(enabled.get());
                    }
                    if (entry == firstEntry) {
                        throw new CommandException(Messages.COMMANDS_MOTD_ERROR_NOCHANGE.t());
                    }

                    config.getMotdEntries().put(motdArg, entry);
                    try {
                        this.config.save();
                    } catch (final ConfigurateException e) {
                        throw new CommandException(Messages.INTERNAL_ERROR_CONFIG_SAVE.t(e.getLocalizedMessage()), e);
                    }

                    ctx.sendMessage(nil(), Messages.COMMANDS_MOTD_EDIT_SUCCESS.t(hl(text().content(motdArg))));

                    return CommandResult.success();
                }).build();
    }

    private Command.Parameterized getMotdDeleteCommand(final Parameter.Value<String> motdParam) {
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_MOTD_DELETE_DESCRIPTION.t())
                .setPermission(Permissions.PERM_MOTD_DELETE)
                .parameter(motdParam)
                .setExecutor(ctx -> {
                    final String motdArg = ctx.requireOne(motdParam);
                    Commands.checkPermission(ctx, Permissions.PERM_MOTD_DELETE, motdArg);
                    final MotdEntry deleted = this.config.getMotdEntries().remove(motdArg);
                    if (deleted == null) {
                        throw new CommandException(Messages.COMMANDS_MOTD_ERROR_DOESNOTEXIST.t(motdArg));
                    }
                    try {
                        this.config.save();
                    } catch (final ConfigurateException e) {
                        throw new CommandException(Messages.INTERNAL_ERROR_CONFIG_SAVE.t(e.getLocalizedMessage()), e);
                    }
                    ctx.sendMessage(nil(), Messages.COMMANDS_MOTD_DELETE_SUCCESS.t(motdArg));
                    return CommandResult.success();
                }).build();
    }
}
