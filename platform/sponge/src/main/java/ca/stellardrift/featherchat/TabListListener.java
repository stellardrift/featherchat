/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.vars.VariableGatherEvent;
import ca.stellardrift.featherchat.vars.Variables;
import com.google.inject.Inject;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Game;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.entity.living.player.tab.TabList;
import org.spongepowered.api.event.*;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.plugin.PluginContainer;

import java.util.List;

public class TabListListener {

    private final Configuration config;
    private final PluginContainer container;
    private final Game game;

    @Inject
    public TabListListener(Configuration config, PluginContainer container, Game game) {
        this.config = config;
        this.container = container;
        this.game = game;
    }

    @Listener
    public void onPlayerJoin(ServerSideConnectionEvent.Join join) {
        final ServerPlayer joined = join.getPlayer();
        TemplateProvider prov = config.templateProvider(joined);
        // Update player's own tab list
        TabList joinedTabList = joined.getTabList();
        joinedTabList.getEntries().forEach(ent -> {
            game.getServer().getPlayer(ent.getProfile().getUniqueId()).map(this::displayName).ifPresent(ent::setDisplayName);
        });

        // Header + Footer
        @Nullable Component header = prov.processToText(TemplateProvider.TEMPLATE_TAB_LIST_HEADER, vars -> populateVars(joined, vars));
        @Nullable Component footer = prov.processToText(TemplateProvider.TEMPLATE_TAB_LIST_FOOTER, vars -> populateVars(joined, vars));
        if (header != null && footer != null) {
            joinedTabList.setHeaderAndFooter(header, footer);
        } else if (header != null) {
            joinedTabList.setHeader(header);
        } else if (footer != null) {
            joinedTabList.setFooter(footer);
        }

        // Update everyone else's tab lists
        final @Nullable Component displayName = displayName(joined);
        if (displayName != null) {
            game.getServer().getOnlinePlayers().forEach(ply -> {
                ply.getTabList().getEntry(joined.getUniqueId()).ifPresent(ent -> ent.setDisplayName(displayName));
            });
        }
    }

    private @Nullable Component displayName(ServerPlayer ply) {
        TemplateProvider prov = this.config.templateProvider(ply);
        return prov.processToText(TemplateProvider.TEMPLATE_TAB_LIST_ENTRY, vars -> populateVars(ply, vars));
    }

    private void populateVars(ServerPlayer ply, List<Object> vars) {
        vars.add(Variables.subject(ply));
        vars.add(Variables.nameable(ply));
        try (CauseStackManager.StackFrame frame = this.game.getServer().getCauseStackManager().pushCauseFrame()) {
            frame.pushCause(ply);
            this.game.getEventManager().post(new VariableGatherEvent(frame.getCurrentCause(), vars));
        }
    }
}
