/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.text.renderer.ComponentRenderer;
import net.kyori.adventure.text.serializer.plain.PlainComponentSerializer;
import net.kyori.adventure.translation.GlobalTranslator;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory2;
import org.apache.logging.log4j.message.ReusableMessageFactory;
import org.apache.logging.log4j.message.ReusableObjectMessage;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Locale;

/**
 * Message factory for log4j
 */
public class TranslatableMessageFactory implements MessageFactory2 {
    private static final ThreadLocal<ComponentMessage> componentMessageThreadLocal = new ThreadLocal<>();
    private final ReusableMessageFactory backing = new ReusableMessageFactory();

    private static ComponentMessage getComponentMessage() {
        @Nullable ComponentMessage message = componentMessageThreadLocal.get();
        if (message == null) {
            componentMessageThreadLocal.set((message = new ComponentMessage()));
        }
        return message;
    }


    @Override
    public Message newMessage(Object message) {
        final ComponentMessage result = getComponentMessage();
        result.set(message);
        return result;
    }

    @Override
    public Message newMessage(String message) {
        return this.backing.newMessage(message);
    }

    @Override
    public Message newMessage(String message, Object... params) {
        return this.backing.newMessage(message, params);
    }

    @Override
    public Message newMessage(CharSequence charSequence) {
        return this.backing.newMessage(charSequence);
    }

    @Override
    public Message newMessage(String message, Object p0) {
        return this.backing.newMessage(message, p0);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1) {
        return this.backing.newMessage(message, p0, p1);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2) {
        return this.backing.newMessage(message, p0, p1, p2);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3) {
        return this.backing.newMessage(message, p0, p1, p2, p3);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4, p5);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4, p5, p6);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4, p5, p6, p7);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    @Override
    public Message newMessage(String message, Object p0, Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9) {
        return this.backing.newMessage(message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    static class ComponentMessage extends ReusableObjectMessage {
        private static final long serialVersionUID = -3477272587468239708L;

        private static final ComponentRenderer<Locale> RENDERER = GlobalTranslator.renderer();
        @Override
        public String getFormattedMessage() {
            final Object param = this.getParameter();
            if (param instanceof ComponentLike) {
                // TODO: ansi formatting
                return PlainComponentSerializer.plain().serialize(RENDERER.render(((ComponentLike) param).asComponent(), Locale.getDefault()));
            }
            return super.getFormattedMessage();
        }

        @Override
        public void formatTo(StringBuilder buffer) {
            final Object param = this.getParameter();
            if (param instanceof ComponentLike) {
                // TODO: ansi formatting
                buffer.append(PlainComponentSerializer.plain().serialize(RENDERER.render(((ComponentLike) param).asComponent(), Locale.getDefault())));
            } else {
                super.formatTo(buffer);
            }
        }
    }
}
