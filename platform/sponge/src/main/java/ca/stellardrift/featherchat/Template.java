/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat;

import ca.stellardrift.featherchat.textxml.XmlComponentSerializer;
import ca.stellardrift.featherchat.vars.VariableGatherEvent;
import ca.stellardrift.featherchat.vars.Variables;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.util.InternalArrayList;
import com.google.auto.value.AutoValue;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.Cause;
import org.spongepowered.api.service.permission.Subject;

import java.io.StringWriter;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * A specification for a template of a certain type
 *
 * @param <C> Context object (player, Cause stack, etc)
 */
@AutoValue
public abstract class Template<C> {

    /**
     * Get the name of the template, used for lookup and error messages
     *
     * @return Template name
     */
    public abstract String templateName();

    public abstract BiConsumer<C, List<Object>> variableDecorator();

    public @Nullable String processToString(TemplateProvider provider, C context) {
        final @Nullable Mustache result = provider.template(templateName());
        if (result == null) {
            return null;
        }
        final List<Object> variables = new InternalArrayList<>();
        variableDecorator().accept(context, variables);
        return processToString(result, variables);
    }

    public @Nullable Component processToText(TemplateProvider provider, C context) {
        final @Nullable String result = processToString(provider, context);
        if (result == null) {
            return null;
        }

        return XmlComponentSerializer.get().deserialize(result);
    }

    public @Nullable String processFragmentToString(TemplateProvider provider, String fragment, C context) {
        final Mustache result = provider.fragment(fragment, templateName());

        final List<Object> variables = new InternalArrayList<>();
        variableDecorator().accept(context, variables);
        return processToString(result, variables);
    }

    public @Nullable Component processFragmentToText(TemplateProvider provider, String fragment, C context) {
        final @Nullable String result = processToString(provider, context);
        if (result == null) {
            return null;
        }

        return XmlComponentSerializer.get().deserialize(result);
    }

    public static <C> Builder<C> builder() {
        return new AutoValue_Template.Builder<>();
    }

    public static Builder<Cause> causeBased() {
        return Template.<Cause>builder()
                .variableDecorator((cause, vars) -> {
                    @Nullable Subject src = cause.first(Subject.class).orElse(null);
                    if (src == null) {
                        return;
                    }
                    vars.add(Variables.subject(src));
                    cause.first(ServerPlayer.class).ifPresent(ply -> vars.add(Variables.player(ply)));
                        VariableGatherEvent event = new VariableGatherEvent(cause, vars); // TODO: Add plugin to cause stack?
                        Sponge.getGame().getEventManager().post(event);
                });
    }

    /**
     * Process the provided template to a string
     * @param template Template to process
     * @return processed template
     */
    static String processToString(Mustache template, List<Object> variables) {
        final StringWriter writer = new StringWriter();
        template.execute(writer, variables);
        return writer.toString();
    }

    @AutoValue.Builder
    public abstract static class Builder<C> {

        public abstract Builder<C> templateName(String name);
        public abstract Builder<C> variableDecorator(BiConsumer<C, List<Object>> variableDecorator);

        public abstract Template<C> build();

    }
}
