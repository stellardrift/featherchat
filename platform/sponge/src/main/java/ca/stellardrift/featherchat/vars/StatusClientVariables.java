/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.vars;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.network.status.StatusClient;

import java.net.InetSocketAddress;

public class StatusClientVariables {
    private final StatusClient client;

    StatusClientVariables(StatusClient client) {
        this.client = client;
    }

    public String clientVersion() {
        return this.client.getVersion().getName();
    }

    public boolean legacy() {
        return this.client.getVersion().isLegacy();
    }

    public String address() {
        return this.client.getAddress().toString();
    }

    public @Nullable String virtualHostName() {
        return this.client.getVirtualHost().map(InetSocketAddress::getHostName).orElse(null);
    }

}
