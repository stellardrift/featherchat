/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.stellardrift.featherchat.channel;

import ca.stellardrift.featherchat.*;
import com.google.common.collect.ImmutableSet;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.exception.CommandException;
import org.spongepowered.api.command.parameter.Parameter;
import org.spongepowered.api.command.parameter.managed.standard.VariableValueParameters;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.Collection;
import java.util.stream.Collectors;

import static net.kyori.adventure.identity.Identity.nil;

/**
 * Commands relating to channels
 */
public class ChannelCommands {
    private ChannelCommands() {
    }

    public static Command.Parameterized createChannelCommands(FeatherChatPlugin plugin) {
        final Parameter.Value<Channel> existingChannel = channel(plugin).setKey("existing-channel").build(); // TODO: Channel or String
        final Parameter.Value<String> newChannel = Parameter.string().setKey("new-channel").build();

        // Commands that take either existing or new
        final Parameter existingOrNew = //Parameter.firstOf(
                Parameter.subcommand(createSetupCommand(plugin, existingChannel, newChannel), "create", "new", "setup", "s", "conf", "edit");
        //);

        // Commands that take existing channel only

        final Parameter existingOnly = Parameter.firstOf(
                Parameter.subcommand(createJoinCommand(plugin, existingChannel), "join", "j"),
                Parameter.subcommand(createLeaveCommand(plugin, existingChannel), "leave", "part", "quit", "l", "p", "q"),
                Parameter.subcommand(createTalkInCommand(plugin, existingChannel), "talk", "t"),
                Parameter.subcommand(createDeleteCommand(plugin, existingChannel), "delete", "del", "d")
        );

        return Command.builder()
                .setShortDescription(Messages.COMMANDS_CHANNEL_DESCRIPTION.t())
                .parameter(Parameter.firstOf(
                        Parameter.seq(Parameter.firstOf(existingChannel, newChannel), existingOrNew),
                        Parameter.seq(existingChannel, existingOnly)
                ))
                .setExecutor(ctx -> {
                    throw new CommandException(Messages.COMMANDS_CHANNEL_ERROR_MISSING_SUBCOMMAND.t());
                })
                .build();

    }

    private static Channel verifyChannel(Object o) throws CommandException {
        if (!(o instanceof Channel)) {
            throw new CommandException(Messages.COMMANDS_CHANNEL_ERROR_NONEXISTANT_CHANNEL.t(o));
        }
        return (Channel) o;
    }

    private static Command.Parameterized createDeleteCommand(final FeatherChatPlugin plugin, final Parameter.Value<Channel> existingChannel) {
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_CHANNEL_DELETE_DESCRIPTION.t())
                .setExecutor(ctx -> {
                    int successes = 0;
                    for (Channel chan : ctx.getAll(existingChannel)) {
                        Commands.checkPermission(ctx, Permissions.PERM_DELETE_CHANNEL, chan.name());
                        plugin.configuration().removeChannel(chan.name());
                        for (Audience member : chan.talkers()) {
                            // TODO: Implement
                            member.sendMessage(Component.text("Your channel would be set here, if that were implemented"));
                            // member.setMessageChannel(plugin.configuration().getDefaultChannel().createInstanceFor(member));
                        }

                        for (Subject receiver : chan.receivers()) {
                            chan.removeSource(receiver);
                        }
                        ++successes;
                        ctx.sendMessage(nil(), Messages.COMMANDS_CHANNEL_DELETE_SUCCESS.t(chan.name()));
                    }
                    return CommandResult.builder().setResult(successes).build();
                })
                .build();
    }

    private static Command.Parameterized createTalkInCommand(final FeatherChatPlugin plugin, final Parameter.Value<Channel> existingChannel) {
        final Parameter.Value<ServerPlayer> targetParam = Parameter.playerOrSource().setKey("target").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_CHANNEL_TALKIN_DESCRIPTION.t())
                .setPermission(Permissions.PERM_TALKIN_CHANNEL)
                .parameter(targetParam)
                .setExecutor(ctx -> {
                    final Channel channel = ctx.requireOne(existingChannel);
                    return new MultiTargetAction<ServerPlayer>(Permissions.forSpecific(Permissions.PERM_TALKIN_CHANNEL, channel.name()), ctx.getCause().getCause(),
                            ctx.getAll(targetParam)) {
                        @Override
                        protected Component getTargetNotificationMessage(ServerPlayer target) {
                            return Messages.COMMANDS_CHANNEL_TALKIN_SUCCESS_TARGET.t(channel.name());
                        }

                        @Override
                        protected Component getSourceNotificationMessage(Collection<ServerPlayer> targets) {
                            return Messages.COMMANDS_CHANNEL_TALKIN_SUCCESS_SOURCE.t(channel.name(), targets.toString());
                        }

                        @Override
                        protected void actSingle(ServerPlayer target) {
                            if (!channel.sendsTo(target)) {
                                channel.addSource(target);
                            }
                            // TODO: Reimplement channels
                            // target.setMessageChannel(channel.createInstanceFor(target));
                        }
                    }.actToResult();
                })
                .build();

    }

    private static Command.Parameterized createLeaveCommand(FeatherChatPlugin plugin, final Parameter.Value<Channel> existingChannel) {
        final Parameter.Value<ServerPlayer> targetParam = Parameter.playerOrSource().setKey("target").build();
        return Command.builder()
                .setShortDescription(Messages.COMMANDS_CHANNEL_LEAVE_DESCRIPTION.t())
                .parameter(targetParam)
                .setExecutor(ctx -> {
                    final Channel channel = ctx.requireOne(existingChannel);
                    return new MultiTargetAction<ServerPlayer>(Permissions.forSpecific(Permissions.PERM_LEAVE_CHANNEL,
                            channel.name()), ctx.getCause().getCause(), ctx.getAll(targetParam)) {
                        @Override
                        protected Component getTargetNotificationMessage(ServerPlayer target) {
                            return Messages.COMMANDS_CHANNEL_LEAVE_SUCCESS_TARGET.t(target, channel.name());
                        }

                        @Override
                        protected Component getSourceNotificationMessage(Collection<ServerPlayer> targets) {
                            return Messages.COMMANDS_CHANNEL_LEAVE_SUCCESS_SOURCE.t(source, channel.name(), targets.stream()
                                    .map(ServerPlayer::getName)
                                    .collect(Collectors.joining(", ")));
                        }

                        @Override
                        protected void actSingle(ServerPlayer target) {
                            channel.removeSource(target);
                        }
                    }.actToResult();
                })
                .build();
    }

    private static Command.Parameterized createJoinCommand(final FeatherChatPlugin plugin, final Parameter.Value<Channel> existingChannel) {
        final Parameter.Value<ServerPlayer> joinTarget = Parameter.playerOrSource().setKey("join").build();
        return Command.builder()
                .parameter(joinTarget)
                .setExecutor(ctx -> {
                    final Channel channel = ctx.requireOne(existingChannel);
                    return new MultiTargetAction<ServerPlayer>(Permissions.forSpecific(Permissions.PERM_JOIN_CHANNEL, channel.name()), ctx.getCause().getCause(),
                            ctx.getAll(joinTarget)) {
                        @Override
                        protected Component getTargetNotificationMessage(ServerPlayer target) {
                            return Messages.COMMANDS_CHANNEL_JOIN_SUCCESS_TARGET.t(target, channel.name());
                        }

                        @Override
                        protected Component getSourceNotificationMessage(Collection<ServerPlayer> targets) {
                            return Messages.COMMANDS_CHANNEL_JOIN_SUCCESS_SOURCE.t(source, channel.name(), targets.stream()
                                    .map(ServerPlayer::getName)
                                    .collect(Collectors.joining(", ")));
                        }

                        @Override
                        protected void actSingle(ServerPlayer target) {
                            channel.addSource(target);
                        }
                    }.actToResult();
                })
                .build();

    }

    private static Command.Parameterized createSetupCommand(final FeatherChatPlugin plugin, final Parameter.Value<Channel> existingParam, final Parameter.Value<String> newParam) {
        return Command.builder()
                .setPermission(Permissions.PERM_EDIT_CHANNEL)
                .setExecutor(ctx -> {
                    final @Nullable Channel existing = ctx.getOne(existingParam).orElse(null);
                    if (existing != null) {
                        throw new CommandException(Messages.COMMANDS_CHANNEL_SETUP_RESPONSE_ALREADY_EXISTS.t(existing.name()));
                    } else {
                        final Channel config = plugin.configuration().getChannel(ctx.requireOne(newParam));
                        try {
                            config.save(plugin.configuration());
                        } catch (SerializationException e) {
                            throw new CommandException(Messages.COMMANDS_CHANNEL_SETUP_RESPONSE_ERROR_SAVE.t(config.name(), e.getMessage()), e);
                        }
                        ctx.sendMessage(nil(), Messages.COMMANDS_CHANNEL_SETUP_RESPONSE_CREATED_NEW.t(config.name()));
                    }
                    // Present parameters
                    return CommandResult.success();
                })
                .build();
    }

    private static Parameter.Value.Builder<Channel> channel(final FeatherChatPlugin plugin) {
        return Parameter.builder(Channel.class, VariableValueParameters.dynamicChoicesBuilder(Channel.class)
                .setChoices(() -> ImmutableSet.copyOf(plugin.configuration().getChannelNames()))
                .setResults(choice -> plugin.configuration().getChannel(choice))
                .build());
    }
}
