/*
 * This file is part of FeatherChat.
 *
 * FeatherChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FeatherChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FeatherChat.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.stellardrift.featherchat;

public class ProjectData {
    public static final String ARTIFACT_ID = "${project.rootProject.name}";
    public static final String NAME = "${project.ext.displayName}";
    public static final String DESCRIPTION = "${project.description}";
    public static final String VERSION = "${project.version}";
    public static final String SOURCE_URL = "${scmUrl}";
    public static final String LICENSE_URL = "${license.url}";
    public static final String GIT_DESCRIBE = "${git}";
}
