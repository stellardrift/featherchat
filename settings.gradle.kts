rootProject.name = "featherchat"

listOf("paper", "sponge").forEach {
    include(":platform:$it")
    findProject(":platform:$it")?.name = "featherchat-$it"
}

// include(":gui")
// findProject(":gui")?.name = "featherchat-gui"
