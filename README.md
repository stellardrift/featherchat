FeatherChat is a comprehensive message modification plugin for Sponge.

[![pipeline status](https://gitlab.com/zml/featherchat/badges/master/pipeline.svg)](https://gitlab.com/zml/featherchat/commits/master) | [Crowdin](https://crowdin.com/project/featherchat) | [Discord](https://discord.gg/B8UucK7) | [Wiki](https://gitlab.com/zml/featherchat/wikis/home) | [Source](https://gitlab.com/zml/featherchat)

File locations
--------------
- `src/main/java`: Main plugin source files
- `src/main/java-templates`: Templates for generated source files
- `src/test/java`: Test source files

Building
--------

We build with Gradle. Because the JAXB api has been removed in JDK 11, the results of a build
 will change based on which java version the plugin is built with, so jars built with JDK 11 
 or later will only run on JDK 11 or later and will have the `-jdk11` suffix added.

`gradle build`. The built jar is located in `build/libs`, ending with -all

License
-------

FeatherChat is released under the terms of the GNU Affero General Public License v3 or later. See the LICENSE file 
for the full terms of the license agreement.
